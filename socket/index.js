import * as config from "./config";
import data, { texts } from "../data";

let usersGame = new Set();
let userList = [];
export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    userList.push(username);
    if(username) {
      io.emit('ADD_USER', {
        name: username,
        users: userList
      })
    }

    socket.on("disconnect", () => {
      userList = [];
      usersGame.delete(username);
    })
    
    socket.on('SEND_LETTER', ()=>{
      io.emit('USERBAR', {name: username})
    })
    socket.on('ADD_TO_GAME', (data)=> {
      usersGame.add(data.name);
    });
    socket.on('DELETE_USER', (data) => {
      usersGame.delete(data.name);
    })

    socket.on('VICTORY', (data)=>{
      io.emit('MODAL', {name: data.name});
    })

    socket.on('READY_CHECK', ()=>{
      io.emit('READY_STATE', {name: username});
      socket.emit('READY_GAME', {name: username});

    });


    socket.on('CHECK_STATUS', () =>{
      if(usersGame.size === userList.length) {
        io.emit('START_GAME', {
          time: config.SECONDS_TIMER_BEFORE_START_GAME,
          text: texts,
          name: username,
          gameTime: config.SECONDS_FOR_GAME
        });
      }
    });
  });
};

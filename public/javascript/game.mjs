import { createElement} from "./helper.mjs";
import { username, readyButton, userBlock, gameField, typingText, gamePage } from './const.mjs';


if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });



readyButton.addEventListener('click', ()=>{
  socket.emit('READY_CHECK', 'ready');

  switch(readyButton.textContent) {
    case 'Ready':
      readyButton.textContent = 'Not Ready';
      socket.on('READY_GAME', (data)=>{  
      socket.emit('ADD_TO_GAME', {name: data.name});
      socket.emit('CHECK_STATUS');
      });
      break;
    case 'Not Ready':
      readyButton.textContent = 'Ready';
      socket.on('READY_GAME', (data)=>{  
      socket.emit('DELETE_USER', {name: data.name});
    });
    break;
  }


});

socket.on('READY_STATE', (data) => {
  const circle = document.querySelector(`.${data.name}`);
  circle.classList.toggle('ready-status-green');
});

function createUser(name) {
  const item = document.createElement('div');
  item.innerHTML = `
  <div class="user_block">
    <div class="${name} ready-status-red" id="circle"></div>
    <div class="name">${name}</div>
    <progress max="18" value="0" class="user-progress ${name} userbar userbar-${name}"></progress>
  </div>
  `
  userBlock.appendChild(item);
}

socket.on('ADD_USER', (data) => {
  userBlock.innerHTML = '';
  const users = data.users;
  users.forEach(el => {
    createUser(el)
  });
});



socket.on('START_GAME', (data)=>{

  const text = data.text[0];
  gameField.innerHTML = '';
  const item = document.createElement('div');
  item.classList.add('timerN');
  gameField.appendChild(item);
  let timerID = setInterval(function intr() {
    item.textContent = data.time;
    data.time--;
    if(data.time === -1){
      clearInterval(timerID);      
      gameField.innerHTML = '';
      gameField.appendChild(typingText);
      let spanText = text.split('');
      for(let i = 0; i<spanText.length; i++) {
        let temp = createElement({
          tagName: 'span',
          className: `word${i}`,
        });
        temp.textContent = spanText[i];
        typingText.appendChild(temp);
        gameField.appendChild(timerContainer);
        fieldTimer();
        
      }
function fieldTimer() {
  let counter = data.gameTime;
  let gameTimer = setInterval(()=>{
    timerContainer.textContent = `${counter--} seconds left`;
  }, 1000)
  setTimeout(()=>{
    clearInterval(gameTimer);
    socket.emit('VICTORY', {name: 'No one'});

  }, 60000);
}
      const letterobj = document.querySelectorAll('.type-text > span');
      const letters = Array.from(letterobj);

      window.addEventListener('keydown', (e)=>{
        if(e.key !== 'Shift' && spanText[0] === e.key){
          console.log(e.key);
            if(spanText[0] === letters[0].textContent){

              letters[0].classList.add('correct');
              letters.shift();
              socket.emit('SEND_LETTER', 'letter');
            }
          
          spanText.shift();
        }

      })
    }
    return intr;
  }(), 1000);
  let timerContainer = createElement({
    tagName: 'div',
    className: 'timer-container'
  });
});

socket.on('USERBAR', (data)=>{
  const userbarProgress = document.querySelector(`.userbar-${data.name}`);
  let value = userbarProgress.getAttribute('value');
  let counter = +value+1;

  userbarProgress.setAttribute('value', counter);
  if(counter === 18) {
    socket.emit('VICTORY', {name: data.name});
    userbarProgress.classList.add('finished');
  }
})

socket.on('MODAL', (data) => {
  let modal = document.createElement('div')
  modal.classList.add('modal-window');
  modal.innerHTML = `
    <h2>Game results</h2>
    <h3 class="winner-name">${data.name}</h1>
  `;
  gameField.innerHTML = '';
  gameField.appendChild(modal);
  gamePage.style.backgroundColor = 'gray';
})
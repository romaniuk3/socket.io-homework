export const username = sessionStorage.getItem("username");
export const readyButton = document.querySelector('.ready-btn');
export const userBlock = document.querySelector('.users');
export const gameField = document.querySelector('.game-field');
export const typingText = document.querySelector('.type-text');
export const gamePage = document.getElementById('game-page');